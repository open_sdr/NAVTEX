/*!
 * \file navtex.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef NAVTEX_H_
#define NAVTEX_H_

// Разнос частот, Гц
#define SDR_NAVTEX_FREQUENCY_SPACING 170

// Символьная скорость, Бод
#define SDR_NAVTEX_SYMBOLS_RATE 100

// Количество отсчётов в символе
#define SDR_NAVTEX_SYMBOL_SIZE(Fd,Decimation) (Fd / Decimation / SDR_NAVTEX_SYMBOLS_RATE)

#endif /* NAVTEX_H_ */
