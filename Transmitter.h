/*!
 * \file Transmitter.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef NAVTEX_TRANSMITTER_H_
#define NAVTEX_TRANSMITTER_H_

#include "SDR/BASE/common.h"
#include "SDR/BASE/Abstract.h"
#include "SDR/BASE/Buffers/ringBuffer.h"
#include "SDR/BASE/Modulation/digital/fskModulator.h"
#include "Codec/Encoder.h"

#include "radio_cfg.h"

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
  ValueEx_t Fd;

  RingBufferInt8Cfg_t Buffer;
  Size_t BufferReadyTriggerLevel;

  Buffer_t generatorBuf;
} NAVTEX_TransmitterCfg_t;

typedef struct
{
  event_sink_t on_buffer_ready;
  event_sink_t on_transmit_finished;
} NAVTEX_TransmitterCallback_t;

typedef struct
{
  void* Parent;

  NAVTEX_Encoder_t Encoder;
  RingBufferInt8_t Buffer;
  FSK_Modulator_t Modulator;

  Int8_t CurrentByte;
  Size_t SymCounter;

  Size_t BufferReadyTriggerLevel;
  event_sink_t on_buffer_ready;
  event_sink_t on_transmit_finished;
} NAVTEX_Transmitter_t;

INLINE Size_t NAVTEX_Transmitter_BufferUsedSpace(NAVTEX_Transmitter_t* This)
{
  return This->Buffer.Count;
}

INLINE Size_t NAVTEX_Transmitter_BufferFreeSpace(NAVTEX_Transmitter_t* This)
{
  return This->Buffer.Size - This->Buffer.Count;
}

INLINE void NAVTEX_Transmitter_setBufferReadyTriggerLevel(NAVTEX_Transmitter_t* This, Size_t Level)
{
  This->BufferReadyTriggerLevel = Level;
}

INLINE void NAVTEX_Transmitter_set_callback(NAVTEX_Transmitter_t* This, void* Parent, NAVTEX_TransmitterCallback_t* Callback)
{
  This->Parent = Parent;
  This->on_buffer_ready = Callback->on_buffer_ready;
  This->on_transmit_finished = Callback->on_transmit_finished;
}

INLINE Size_t navtex_transmit_1(NAVTEX_Transmitter_t* This, Int8_t Data)
{
  if ( (This->Buffer.Count + 2) > This->Buffer.Size )
    return 0;

  NAVTEX_Code_t Code;
  navtex_encoder_1(&This->Encoder, Data, Code);
  RingBufferInt8_write(&This->Buffer, (Int8_t*)&Code, 2);
  return 1;
}

INLINE Size_t navtex_transmit(NAVTEX_Transmitter_t* This, Int8_t* Data, Size_t Count)
{
  Size_t i;
  for (i = 0; i < Count; i++)
    if (!navtex_transmit_1(This, Data[i]))
      break;
  return i;
}

void init_NAVTEX_Transmitter(NAVTEX_Transmitter_t* This, NAVTEX_TransmitterCfg_t* cfg);

INLINE void navtex_transmitter(NAVTEX_Transmitter_t* This, iqSample_t* Samples, Size_t Count)
{
  fsk_modulator_iq(&This->Modulator, Samples, Count);
}

#ifdef __cplusplus
}
#endif

#endif /* NAVTEX_TRANSMITTER_H_ */
