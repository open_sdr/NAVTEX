/*!
 * \file Receiver.c
 * \author Андрей Шиганцов
 * \brief
 */

#include "Receiver.h"

static Overflow_t samples_processor(NAVTEX_Receiver_t* This, Sample_t* Samples, Size_t Count, Sample_t* Result)
{
  Overflow_t res = filter(&This->DemodulatorFilter, Samples, Count, Result);

  if (This->demodulated_signal_sink)
    This->demodulated_signal_sink(This->Parent, Result, Count);

  return res;
}

static void bit_sink(NAVTEX_Receiver_t* This, Bit_t Bit)
{
  bits_collector32(&This->Collector, Bit);

  if (This->detector_bit_sink)
    This->detector_bit_sink(This->Parent, Bit);
}

static void on_collector_ready(NAVTEX_Receiver_t* This, uint32_t buf)
{
  if (This->isSync)
  {
    NAVTEX_Code_t code;
    code[0] = ( buf >> (2*NAVTEX_CODE_SIZE) ) & NAVTEX_CODE_MASK;
    code[1] = ( buf >> (3*NAVTEX_CODE_SIZE) ) & NAVTEX_CODE_MASK;
    navtex_decoder_1(&This->Decoder, (Int8_t*)&code);
  }
}

static void on_collector_sync(NAVTEX_Receiver_t* This, uint32_t buf)
{
  SDR_UNUSED(buf);
  This->isSync = 1;
}

static void on_decoder_start(NAVTEX_Receiver_t* This)
{
  This->Counter = 0;

  if (This->on_message_start)
    This->on_message_start(This->Parent);
}

static void on_decoder_end(NAVTEX_Receiver_t* This)
{
  This->isSync = 0;

  if (This->Counter != 0)
  {
    if (This->on_buf_ready)
      This->on_buf_ready(This->Parent, This->Buf, This->Counter);

    This->Counter = 0;
  }

  if (This->on_message_end)
    This->on_message_end(This->Parent);
}

static void on_decoder_char(NAVTEX_Receiver_t* This, NAVTEX_DecoderChar_t* Char)
{
  SDR_ASSERT(This->Counter <= This->BufSize);
  This->Buf[This->Counter++] = *Char;
  if (This->Counter == This->BufSize)
  {
    if (This->on_buf_ready)
      This->on_buf_ready(This->Parent, This->Buf, This->BufSize);

    This->Counter = 0;
  }
}

void init_NAVTEX_Receiver(NAVTEX_Receiver_t* This, NAVTEX_ReceiverCfg_t* cfg)
{
  This->Parent = 0;
  This->demodulated_signal_sink = 0;
  This->detector_bit_sink = 0;
  This->on_message_start = 0;
  This->on_message_end = 0;
  This->on_buf_ready = 0;

  FSK_DemodulatorCfg_t cfgDetector;
  cfgDetector.Fd = cfg->Fd;
  cfgDetector.DecimationCoeff = cfg->Decimation;
  cfgDetector.Freq0 = SDR_NAVTEX_FREQUENCY_SPACING/2;
  cfgDetector.Freq1 = -SDR_NAVTEX_FREQUENCY_SPACING/2;
  cfgDetector.BaudRate = SDR_NAVTEX_SYMBOLS_RATE;
  cfgDetector.OffsetFilter = cfg->SyncOffsetFilter;
  cfgDetector.OffsetTreshold = cfg->SyncOffsetTreshold;
  cfgDetector.SyncWindowBuf = cfg->SyncWindowBuf;
  cfgDetector.SaturationBuf = cfg->SaturationBuf;
  init_FSK_Demodulator(&This->Detector, &cfgDetector);

  FSK_DemodulatorCallback_t callbackDetector;
  callbackDetector.user_processor = (samples_processor_t)samples_processor;
  callbackDetector.bit_processor = (bit_sink_t)bit_sink;
  FSK_Demodulator_set_callback(&This->Detector, This, &callbackDetector);

  init_Filter(&This->DemodulatorFilter, &cfg->DemodulatorFilter);

  BitsCollector32Cfg_t cfgCollector;
  cfgCollector.BitsCount = 2*NAVTEX_CODE_SIZE;
  cfgCollector.BitsSize = 4*NAVTEX_CODE_SIZE;
  cfgCollector.SyncErrorsThreshold = cfg->BitsSyncErrorsThreshold;
  cfgCollector.syncSequence = NAVTEX_CODE_ALPHA;
  cfgCollector.syncSequence <<= NAVTEX_CODE_SIZE;
  cfgCollector.syncSequence |= NAVTEX_CODE_RQ;
  cfgCollector.syncSequence <<= NAVTEX_CODE_SIZE;
  cfgCollector.syncSequence |= NAVTEX_CODE_ALPHA;
  cfgCollector.syncSequence <<= NAVTEX_CODE_SIZE;
  cfgCollector.syncSequence |= NAVTEX_CODE_RQ;
  init_BitsCollector32(&This->Collector, &cfgCollector);

  BitsCollector32Callback_t callbackCollector;
  callbackCollector.on_ready = (BitsCollector32_on_ready_t)on_collector_ready;
  callbackCollector.on_sync = (BitsCollector32_on_sync_t)on_collector_sync;
  BitsCollector32_set_callback(&This->Collector, This, &callbackCollector);

  NAVTEX_DecoderCfg_t cfgDecoder;
  cfgDecoder.AlphaCountForEnd = cfg->AlphaCountForEnd;
  init_NAVTEX_Decoder(&This->Decoder, &cfgDecoder);

  NAVTEX_DecoderCallback_t callbackDecoder;
  callbackDecoder.on_start = (event_sink_t)on_decoder_start;
  callbackDecoder.on_char = (NAVTEX_Decoder_char_sink)on_decoder_char;
  callbackDecoder.on_end = (event_sink_t)on_decoder_end;
  NAVTEX_Decoder_set_callback(&This->Decoder, This, &callbackDecoder);

  This->Buf = cfg->Buf;
  This->BufSize = cfg->BufSize;

  NAVTEX_Receiver_reset(This);
}
