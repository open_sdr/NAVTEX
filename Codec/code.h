/*!
 * \file code.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef CODE_H_
#define CODE_H_

#include "SDR/BASE/common.h"
#include "ita2.h"

#ifdef __cplusplus
extern "C"
{
#endif

// Количество бит в байте
#define NAVTEX_CODE_SIZE 7
#define NAVTEX_CODE_MASK 0x7F

// Возврат каретки
#define NAVTEX_CODE_CARRIAGE_RETURN	0x07	// YYYBBBB
// Перевод строки
#define NAVTEX_CODE_LINE_FEED		0x13	// YYBBYBB
// Пробел
#define NAVTEX_CODE_SPACE			0x23	// YYBBBYB
// Нет информации
#define NAVTEX_CODE_NO_INF			0x15	// YBYBYBB

// Холостой сигнал Бетта
#define NAVTEX_CODE_BETA	0x4C	// BBYYBBY
// Фазирующий сигнал 1, холостой сигнал Альфа
#define NAVTEX_CODE_ALPHA	0x70	// BBBBYYY
// Фазирующий сигнал 2
#define NAVTEX_CODE_RQ		0x19	// YBBYYBB
// Флаг ошибки
#define NAVTEX_CODE_ERROR	-1

// Синхропоследовательность
#define NAVTEX_SEQUENCE_SYNC	0x81
// Завершающая последовательность
#define NAVTEX_SEQUENCE_END		0x82

typedef Int8_t NAVTEX_Code_t[2];

// Возвращает код символа алфавита №2 в передаваемом сигнале
Int8_t navtex_encode(Int8_t ita2Char);
// Возвращает символа алфавита №2 из кода передаваемого сигнала
Int8_t navtex_decode(Int8_t Code);
// Возвращает номер передаваемого кода или -1(ошибка)
Int8_t navtex_code_index(Int8_t Code);

INLINE Bool_t navtex_code_is_system(Int8_t Code)
{
  return (Code == NAVTEX_CODE_RQ)||(Code == NAVTEX_CODE_ALPHA)||(Code == NAVTEX_CODE_BETA);
}

INLINE Bool_t navtex_char_is_sequence(Int8_t Char)
{
  return ((UInt8_t)Char == NAVTEX_SEQUENCE_SYNC)||((UInt8_t)Char == NAVTEX_SEQUENCE_END);
}

// Передаваемый 7-элементный сигнал
extern const Int8_t NAVTEX_CodeTabel [ITA2_CHARACTERS_COUNT];

#ifdef __cplusplus
}
#endif

#endif /* CODE_H_ */
