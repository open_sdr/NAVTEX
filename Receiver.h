/*!
 * \file Receiver.h
 * \author Андрей Шиганцов
 * \brief
 */

#ifndef NAVTEX_RECEIVER_H_
#define NAVTEX_RECEIVER_H_

#include "SDR/BASE/common.h"
#include "SDR/BASE/Modulation/digital/fskDemodulator.h"
#include "SDR/BASE/Solvers/bitsCollector.h"
#include "Codec/Decoder.h"

#include "radio_cfg.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define SDR_NAVTEX_SYNC_WINDOW_BUFSIZE(Fd,Decimation) SDR_FSK_SYNC_WINDOW_BUFSIZE(Fd,Decimation,SDR_NAVTEX_SYMBOLS_RATE)

typedef struct
{
  ValueEx_t Fd;
  Value_t Decimation;

  FilterCfg_t DemodulatorFilter;

  FilterCfg_t SyncOffsetFilter;
  Size_t SyncOffsetTreshold;
  Int8_t BitsSyncErrorsThreshold;
  Int8_t AlphaCountForEnd;

  Buffer_t SyncWindowBuf;
  Buffer_t SaturationBuf;

  NAVTEX_DecoderChar_t* Buf;
  Size_t BufSize;
} NAVTEX_ReceiverCfg_t;

typedef void (*NAVTEX_Receiver_chars_sink_t)(void* This, NAVTEX_DecoderChar_t* Chars, Size_t Count);

typedef struct
{
  event_sink_t on_message_start;
  event_sink_t on_message_end;
  NAVTEX_Receiver_chars_sink_t on_buf_ready;

  samples_sink_t demodulated_signal_sink;
  bit_sink_t detector_bit_sink;
} NAVTEX_ReceiverCallback_t;

typedef struct
{
  void* Parent;

  FSK_Demodulator_t Detector;
  BitsCollector32_t Collector;
  NAVTEX_Decoder_t Decoder;

  NAVTEX_DecoderChar_t* Buf;
  Size_t BufSize;
  Size_t Counter;

  Filter_t DemodulatorFilter;
  Bool_t isSync;

  samples_sink_t demodulated_signal_sink;
  bit_sink_t detector_bit_sink;
  event_sink_t on_message_start;
  event_sink_t on_message_end;
  NAVTEX_Receiver_chars_sink_t on_buf_ready;
} NAVTEX_Receiver_t;

void init_NAVTEX_Receiver(NAVTEX_Receiver_t* This, NAVTEX_ReceiverCfg_t* cfg);

INLINE void NAVTEX_Receiver_reset(NAVTEX_Receiver_t* This)
{
  This->isSync = 0;
  This->Counter = 0;
  NAVTEX_Decoder_reset(&This->Decoder);
}

INLINE void NAVTEX_Receiver_set_callback(NAVTEX_Receiver_t* This, void* Parent, NAVTEX_ReceiverCallback_t* Callback)
{
  This->Parent = Parent;
  This->on_message_start = Callback->on_message_start;
  This->on_message_end = Callback->on_message_end;
  This->on_buf_ready = Callback->on_buf_ready;
  This->demodulated_signal_sink = Callback->demodulated_signal_sink;
  This->detector_bit_sink = Callback->detector_bit_sink;
}

INLINE void navtex_receiver(NAVTEX_Receiver_t* This, iqSample_t* Samples, Size_t Count)
{
  fsk_demodulator_iq(&This->Detector, Samples, Count);
}

#ifdef __cplusplus
}
#endif

#endif /* NAVTEX_RECEIVER_H_ */
